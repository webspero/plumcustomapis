const ShopifyAuthcation = require('./Helper');
const getCouponDetails = async(req,res) =>{
    try {
        const code = req.body;
        const GetCouponDetails = await ShopifyAuthcation.discountCode.lookup(code);
        if(!GetCouponDetails){return res.send({"message":"No coupon found"})};
        const couponData = await ShopifyAuthcation.priceRule.get(GetCouponDetails.price_rule_id);
        res.send(couponData);
    } catch (error) {
        res.send({"message":"No coupon found",error});
        console.log('error',error);
        return false;
    } 
    
}

module.exports = {
    getCouponDetails
}