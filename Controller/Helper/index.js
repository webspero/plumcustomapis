const shopifyAPI = require('shopify-api-node');
const {shopName,accessToken} = process.env;
const ShopifyAuth = new shopifyAPI({
    shopName: shopName, // MYSHOP.myshopify.com
    accessToken: accessToken // Your API password
  });
module.exports = ShopifyAuth;