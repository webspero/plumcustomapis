const express = require('express');
const discount = require('./discount');
const router = express.Router();

router.use('/discount', discount);

module.exports = router